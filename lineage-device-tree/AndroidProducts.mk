#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_ROGA_sprout.mk

COMMON_LUNCH_CHOICES := \
    lineage_ROGA_sprout-user \
    lineage_ROGA_sprout-userdebug \
    lineage_ROGA_sprout-eng
