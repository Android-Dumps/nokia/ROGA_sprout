#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from ROGA_sprout device
$(call inherit-product, device/hmd/ROGA_sprout/device.mk)

PRODUCT_DEVICE := ROGA_sprout
PRODUCT_NAME := lineage_ROGA_sprout
PRODUCT_BRAND := Nokia
PRODUCT_MODEL := Nokia G10
PRODUCT_MANUFACTURER := hmd

PRODUCT_GMS_CLIENTID_BASE := android-hmd

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="sys_mssi_rog-user 13 TP1A.220624.014 eng.jenkin.20230322.160532 release-keys"

BUILD_FINGERPRINT := Nokia/Rogue_00WW/ROGA_sprout:12/SP1A.210812.016/00WW_3_300:user/release-keys
