#
# Copyright (C) 2023 The Android Open Source Project
# Copyright (C) 2023 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_ROGA_sprout.mk

COMMON_LUNCH_CHOICES := \
    omni_ROGA_sprout-user \
    omni_ROGA_sprout-userdebug \
    omni_ROGA_sprout-eng
