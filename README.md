## sys_mssi_rog-user 13 TP1A.220624.014 eng.jenkin.20230322.160532 release-keys
- Manufacturer: hmd global
- Platform: mt6765
- Codename: ROGA_sprout
- Brand: Nokia
- Flavor: sys_mssi_rog-user
- Release Version: 13
- Kernel Version: 4.19.191
- Id: TP1A.220624.014
- Incremental: 00WW_3_300
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: Nokia/Rogue_00WW/ROGA_sprout:12/SP1A.210812.016/00WW_3_300:user/release-keys
- OTA version: 
- Branch: sys_mssi_rog-user-13-TP1A.220624.014-eng.jenkin.20230322.160532-release-keys
- Repo: nokia/ROGA_sprout
